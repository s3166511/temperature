package nl.utwente.di.toFahrenheit;

import java.util.HashMap;

public class Quoter {
    public double getFahrenheit(Integer celsius) {
        return (celsius*1.8) + 32;
    }
}
