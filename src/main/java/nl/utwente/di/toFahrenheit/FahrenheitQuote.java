package nl.utwente.di.toFahrenheit;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class FahrenheitQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private nl.utwente.di.toFahrenheit.Quoter quoter;
	
    public void init() throws ServletException {
    	quoter = new nl.utwente.di.toFahrenheit.Quoter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Fahrenheit Quote";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   quoter.getFahrenheit(Integer.valueOf(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
  }
}
